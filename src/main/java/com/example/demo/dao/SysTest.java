package com.example.demo.dao;

import lombok.Data;

@Data
public class SysTest {
    private Long testId;
    private String testContent;
}
