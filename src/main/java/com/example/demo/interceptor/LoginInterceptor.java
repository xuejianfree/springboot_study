package com.example.demo.interceptor;

import com.alibaba.fastjson.JSON;
import com.example.demo.service.LoginService;
import com.example.demo.vo.ErrorCode;
import com.example.demo.vo.Result;
import io.netty.util.internal.StringUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
@Slf4j
public class LoginInterceptor implements HandlerInterceptor {

    @Autowired
    private LoginService loginService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        if(!(handler instanceof HandlerMethod)) {
            return false;
        }

        String token = request.getHeader("token");
        String uri = request.getRequestURI();

        log.info("=================request start===========================");
        log.info("request uri:{}", uri);
        log.info("token:{}", token);
        log.info("=================request end===========================");

        // 请求头无 token
        if(StringUtils.isBlank(token)) {
            Result result = Result.fail(ErrorCode.NO_LOGIN.getCode(), "未登录");
            response.setContentType("application/json;charset=utf-8");
            response.getWriter().print(JSON.toJSONString(result));
            return false;
        }

        Long userId = loginService.checkToken(token);
        if(userId == null) {
            Result result = Result.fail(ErrorCode.NO_LOGIN.getCode(), "未登录1");
            response.setContentType("application/json;charset=utf-8");
            response.getWriter().print(JSON.toJSONString(result));
            return false;
        }

        return  true;
    }

}
