package com.example.demo.service;

import com.example.demo.dao.SysTest;

import java.util.List;

public interface SysTestService {
    List<SysTest> selectList();
}
