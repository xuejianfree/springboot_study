package com.example.demo.service;

import com.example.demo.vo.params.LoginParam;
import com.example.demo.vo.Result;

public interface LoginService {

    Result login(LoginParam loginParam);

    Long checkToken(String token);

    Result logout(String token);
}
