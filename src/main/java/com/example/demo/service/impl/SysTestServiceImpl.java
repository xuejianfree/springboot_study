package com.example.demo.service.impl;

import com.example.demo.dao.SysTest;
import com.example.demo.mapper.SysTestMapper;
import com.example.demo.service.SysTestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SysTestServiceImpl implements SysTestService {

    @Autowired
    private SysTestMapper sysTestMapper;

    @Override
    public List<SysTest> selectList() {
        return sysTestMapper.selectList();
    }
}
