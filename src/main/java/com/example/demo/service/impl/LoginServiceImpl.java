package com.example.demo.service.impl;

import com.alibaba.fastjson.JSON;
import com.example.demo.service.LoginService;
import com.example.demo.utils.JWTUtils;
import com.example.demo.vo.params.LoginParam;
import com.example.demo.vo.Result;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.concurrent.TimeUnit;

@Service
public class LoginServiceImpl implements LoginService {

    @Autowired(required = false)
    private RedisTemplate<String, String> redisTemplate;

    @Override
    public Result login(LoginParam loginParam) {

        String account = loginParam.getAccount();
        String password = loginParam.getPassword();
        if(account == null || password == null){
            return Result.fail(400, "参数错误");
        }

        // 生成 token
        String token = JWTUtils.createToken(1L);
        // 存入 redis
        // TODO 如果 redis 中 token 有效，就直接返回
        String userIdJson = redisTemplate.opsForValue().get("TOKEN_" + token);
        if(StringUtils.isBlank(userIdJson)) {

        }

        redisTemplate.opsForValue().set("TOKEN_" + token, JSON.toJSONString(1L), 30, TimeUnit.MINUTES);
        return Result.success(token);
    }

    /* 校验 token
    * 1. token 是否可以解出
    * 2. 能解出，redis 中找
    * */
    @Override
    public Long checkToken(String token) {
        if(StringUtils.isBlank(token)) {
            return null;
        }


        Map<String, Object> map = JWTUtils.checkToken(token);
        if(map == null) {
            return null;
        }

        System.out.println("Loginservice1 " + token);

        String userIdJson = redisTemplate.opsForValue().get("TOKEN_" + token);
        System.out.println("Loginservice2 " + userIdJson);
        if(StringUtils.isBlank(userIdJson)) {
            return null;
        }


        Long userId = JSON.parseObject(userIdJson, Long.class);
        return userId;
    }

    @Override
    public Result logout(String token) {
        redisTemplate.delete("TOKEN_" + token);
        return Result.success(null);
    }
}
