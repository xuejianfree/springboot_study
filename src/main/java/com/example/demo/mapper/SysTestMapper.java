package com.example.demo.mapper;

import com.example.demo.dao.SysTest;

import java.util.List;

public interface SysTestMapper {
    public List<SysTest> selectList();
}
