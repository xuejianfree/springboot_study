package com.example.demo.controller;

import com.example.demo.dao.SysTest;
import com.example.demo.service.SysTestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class TestController {

    @Autowired
    SysTestService sysTestService;

    @RequestMapping("test")
    public String test() {
        return "test";
    }

    @GetMapping("/list")
    public List<SysTest> getList() {
        return sysTestService.selectList();
    }
}
