package com.example.demo.controller;

import com.example.demo.service.LoginService;
import com.example.demo.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LogoutController {
    @Autowired
    private LoginService loginService;

    @GetMapping("logout")
    public Result logout(@RequestHeader("token") String token) {
        return loginService.logout(token);
    }
}
