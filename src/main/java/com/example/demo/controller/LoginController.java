package com.example.demo.controller;

import com.example.demo.service.LoginService;
import com.example.demo.vo.params.LoginParam;
import com.example.demo.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoginController {

    @Autowired
    private LoginService loginService;

    @PostMapping("/login")
    public Result Login(@RequestBody LoginParam loginParam) {
        return loginService.login(loginParam);
    }
}
